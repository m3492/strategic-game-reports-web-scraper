import sys
import os
import pandas as pd
import requests
import re
from bs4 import BeautifulSoup


class WebScraper:

    def __init__(self, quarterly_reports_urls, annual_reports_urls):
        self.quarterly_reports_urls = quarterly_reports_urls
        self.annual_reports_urls = annual_reports_urls

        self.target_path = "C:/SMA"
        self.sales_path = "/sales"
        self.production_path = "/production"
        self.supply_path = "/supply"
        self.hr_path = "/hr"
        self.cash_flow_path = "/cash-flow"
        self.balance_sheet_path = "/balance-sheet"
        self.company_value_path = "/company-value"
        self.competitors_values_path = "/competitors-values"
        self.profit_and_loss_account_path = "/profit-and-loss-account"
        self.annual_path = "/annual"

        self.target_folder_paths = [
            self.target_path,
            self.target_path + self.sales_path,
            self.target_path + self.production_path,
            self.target_path + self.supply_path,
            self.target_path + self.hr_path,
            self.target_path + self.cash_flow_path,
            self.target_path + self.balance_sheet_path,
            self.target_path + self.company_value_path,
            self.target_path + self.competitors_values_path,
            self.target_path + self.profit_and_loss_account_path,
            self.target_path + self.annual_path
        ]

        self.csv_suffix = '.csv'
        self.file_paths = []
        # file paths
        self.init_file_paths()
        # prepare target folders
        self.prepare_csv_folders()
        # init table index
        self.table_idx = 0
        self.tables = []
        self.table_group_idx = 0

    def init_file_paths(self):
        for i in range(1, len(self.target_folder_paths)):
            self.file_paths.append(self.target_folder_paths[i] + "/" + self.target_folder_paths[i].split("/")[-1])

    def prepare_csv_folders(self):
        for folder_path in self.target_folder_paths:
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)

    def increment_table_idx(self):
        self.table_idx += 1

    def increment_table_group_idx(self):
        self.table_group_idx += 1

    def get_table_th_text_values(self):
        th_values = []
        th_elements = self.tables[self.table_idx].find_all("th")
        for th_element in th_elements:
            th_values.append(th_element.get_text().strip())
        self.increment_table_idx()
        return th_values

    def get_table_first_th_text_value(self):
        return self.tables[self.table_idx].find("th").get_text().strip()

    def get_table_td_numeric_values(self):
        td_values = []
        td_elements = self.tables[self.table_idx].find_all("td")
        for td_element in td_elements:
            text_value = td_element.get_text().strip()
            if text_value == 'no':
                td_values.append(0)
            elif text_value == 'yes':
                td_values.append(1)
            elif not text_value:
                td_values.append('NULL')
            # if matches float regexp, retype to float
            elif re.match(r'^-?\d+(?:\.\d+)$', text_value):
                td_values.append(float(text_value))
            elif isinstance(text_value, int):
                td_values.append(int(text_value))
            # else retype to int
            else:
                td_values.append(text_value)
        self.increment_table_idx()
        return td_values

    def get_df_by_3_cols_table_group(self):
        table_columns = [self.get_table_th_text_values()]
        for i in range(0, 2):
            column = [self.get_table_first_th_text_value()]
            column = column + self.get_table_td_numeric_values()
            table_columns.append(column)
        return pd.DataFrame(table_columns)

    def get_df_by_2_cols_table_group(self):
        table_columns = [
            self.get_table_th_text_values(),
            self.get_table_td_numeric_values()]
        self.increment_table_group_idx()
        return pd.DataFrame(table_columns)

    def get_df_by_4_cols_table_group(self):
        table_columns = [
            self.get_table_th_text_values(),
            self.get_table_td_numeric_values(),
            self.get_table_th_text_values(),
            self.get_table_td_numeric_values()]
        self.increment_table_group_idx()
        return pd.DataFrame(table_columns)

    def get_annual_df_by_n_cols_table_group(self, n):
        table_columns = [self.get_table_th_text_values()]
        for i in range(0, n - 1):
            column = [self.get_table_first_th_text_value()]
            column = column + self.get_table_td_numeric_values()
            table_columns.append(column)
        return pd.DataFrame(table_columns)

    def write_transposed_df_to_csv(self, file_path_idx, quarter_idx, df):
        df = df.transpose()
        file_path = self.file_paths[file_path_idx] + "-" + str(quarter_idx + 1) + self.csv_suffix
        df.to_csv(file_path, header=False, index=False, sep=";")

    def write_csv_by_n_cols_table_group(self, num_of_cols, file_path_idx, quarter_idx):
        if num_of_cols == 2:
            df = self.get_df_by_2_cols_table_group()
        elif num_of_cols == 3:
            df = self.get_df_by_3_cols_table_group()
        elif num_of_cols == 4:
            df = self.get_df_by_4_cols_table_group()
        else:
            raise ValueError('Unsupported number of columns:' + str(num_of_cols))
        self.write_transposed_df_to_csv(file_path_idx, quarter_idx, df)

    def write_annual_csv_by_n_cols_table_group(self, num_of_cols, file_path_idx, annual_idx):
        df = self.get_annual_df_by_n_cols_table_group(num_of_cols)
        self.write_transposed_df_to_csv(file_path_idx, annual_idx, df)

    def generate_csv_files_by_web_quarters(self):
        for quarter_idx, url in enumerate(self.quarterly_reports_urls):

            page = requests.get(url)
            soup = BeautifulSoup(page.content, 'html.parser')
            self.tables = soup.find_all("table")

            # reset HTML tables indexes
            self.table_idx = 0
            self.table_group_idx = 0
            # process 3-column HTML table groups 3-times
            # sales, production, supply
            for i in range(0, 3):
                self.write_csv_by_n_cols_table_group(3, i, quarter_idx)
            # process 2-column HTML table groups 2-times
            # HR, cash flow
            for i in range(3, 5):
                self.write_csv_by_n_cols_table_group(2, i, quarter_idx)
            # process 4-column HTML table group once
            # balance sheet
            self.write_csv_by_n_cols_table_group(4, 5, quarter_idx)
            # process 2-column HTML table groups 3-times
            # Company value, Values of companies on the market, Profit and loss account
            for i in range(6, 9):
                self.write_csv_by_n_cols_table_group(2, i, quarter_idx)

    def generate_csv_files_by_annual_reports(self):

        for annual_idx, url in enumerate(self.annual_reports_urls):
            self.table_idx = 0
            self.table_group_idx = 0

            page = requests.get(url)
            soup = BeautifulSoup(page.content, 'html.parser')
            self.tables = soup.find_all("table")
            self.write_annual_csv_by_n_cols_table_group(len(self.tables), 9, annual_idx)







