from WebScraper import WebScraper

quarterly_reports_urls = [
    "FILL_ME"
]

annual_reports_urls = [
    "FILL_ME"
]

web_scraper = WebScraper(quarterly_reports_urls, annual_reports_urls)
web_scraper.generate_csv_files_by_web_quarters()
web_scraper.generate_csv_files_by_annual_reports()
